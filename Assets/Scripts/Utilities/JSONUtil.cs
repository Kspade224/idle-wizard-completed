using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class JSONUtil
{
    public static void WrileFile(string filename, string content)
    {
        File.WriteAllText(filename, content);
    }

    public static T LoadDataFromJson<T>(string filename)
    {
        TextAsset textFile = (TextAsset)Resources.Load(filename, typeof(TextAsset));
        //bool fileExist = File.Exists("Assets);
        string fileContent = textFile.ToString();

        return JsonUtility.FromJson<T>(fileContent);
    }

    // From Internet

    public static T[] FromJsonArray<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJsonArray<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJsonArray<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    public static string ToJsonArray<T>(List<T> array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array.ToArray();
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}
