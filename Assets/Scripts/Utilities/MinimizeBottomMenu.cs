using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimizeBottomMenu : MonoBehaviour
{
    public GameObject BotomMenu;
    public GameObject MaxMinButton;
    public float MinimizeRate = 4;
    private RectTransform BotomMenuRT; 
    private bool Minimized=true;
    private Vector2 rectSize;
    private float offset;
    // Start is called before the first frame update
    void Start()
    {
        BotomMenuRT=BotomMenu.GetComponent<RectTransform>();
        rectSize = BotomMenuRT.sizeDelta;
        Vector2 TargetNewPos = BotomMenu.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition;
        offset= TargetNewPos.y - rectSize.y/2;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MinimizeMenu(){
        rectSize = new Vector2 (rectSize.x, rectSize.y/MinimizeRate);
        MaxMinButton.transform.Rotate(0, 0, 180);
        ReducePosition(BotomMenu.transform.GetChild (0) , MinimizeRate);
        // ReducePosition(BotomMenu.transform.GetChild (1) , MinimizeRate);

        BotomMenuRT.sizeDelta = rectSize;
    }

    public void MaximizeMenu(){
        rectSize = new Vector2 (rectSize.x, rectSize.y*MinimizeRate);
        MaxMinButton.transform.Rotate(0, 0, 180);
        IncreasePosition(BotomMenu.transform.GetChild (0) , MinimizeRate);
        // IncreasePosition(BotomMenu.transform.GetChild (1) , MinimizeRate);

        BotomMenuRT.sizeDelta = rectSize;
    }

    private void ReducePosition(Transform target, float rate ){     
        Vector2 TargetNewPos = target.GetComponent<RectTransform>().anchoredPosition;
        TargetNewPos  = new Vector2(TargetNewPos.x,(TargetNewPos.y-offset)/rate+offset);
        target.GetComponent<RectTransform>().anchoredPosition = TargetNewPos;
    }

    private void IncreasePosition(Transform target, float rate ){
        Vector2 TargetNewPos = target.GetComponent<RectTransform>().anchoredPosition;
        TargetNewPos  = new Vector2(TargetNewPos.x,(TargetNewPos.y-offset)*rate+offset);
        target.GetComponent<RectTransform>().anchoredPosition = TargetNewPos;
    }
    public void MaxMinMenuToggle()
    {
        if (Minimized)
        {
            MaximizeMenu();
        }
        else MinimizeMenu();
        Minimized = !Minimized;
    }
}
