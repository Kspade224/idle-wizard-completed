using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardShotController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    [SerializeField]
    private GameObject shotContactObject;

    [SerializeField]
    private float moveSpeed;

    public string shotDamage;

    /** ======= MARK: - MonoBehavior Functions ======= */

    void Start()
    {
        //AddListeners();
        shotDamage = GameFlowManager.instance.gameObject.GetComponent<WizardShotUpgradeController>().currentWizardShotPower;
    }

    void Update()
    {
        Vector3 targetedPosition = GameFlowManager.instance.targetShotPoint.transform.position;
        float flyStep = moveSpeed * Time.deltaTime;

        if (Vector3.Distance(transform.position, targetedPosition) > flyStep)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetedPosition, flyStep);
        }
        else
        {
            if (GUIManager.instance.gameObject.GetComponent<BackgroundNextStageController>().isCameraMoveRunning)
            {
                transform.gameObject.SetActive(false);
                return;
            }

            SoundManager.instance.wizardShotContactSFXSource.PlayOneShot(SoundManager.instance.soundList.wizardShotContactSFX);
            transform.gameObject.SetActive(false);
            shotContactObject.SetActive(true);

            shotDamage = GameFlowManager.instance.gameObject.GetComponent<WizardShotUpgradeController>().currentWizardShotPower;

            CustomEventSystem.instance.DispatchEvent(EventCode.ON_SHOT_CONTACT, new object[] {
                ShotType.WIZARD,
                shotDamage
            });
        }
    }
    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {

        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Actions ======= */

    /** ======= MARK: - Triggers ======= */

}
